from app import app
import os
from flask import request, jsonify
from flask_cors import CORS
from predict_test import InferenceConfig, modellib, CocoLikeDataset
import numpy as np
import codecs, json
import skimage
from numpyEncoder import NumpyEncoder


CORS(app)
inference_config = InferenceConfig()
model = modellib.MaskRCNN(mode="inference",
                          config=inference_config,
                          model_dir="./mask_rcnn_walls_0008.h5")
dataset_val = CocoLikeDataset()
model.load_weights("mask_rcnn_walls_0008.h5", by_name=True)


@app.route("/api/get-masks", methods=["GET", "POST"])
def get_masks():
    file = request.files.get("photo")
    if not file:
        return {"error": "No file"}, 400

    img = skimage.io.imread(file)



    img_arr = np.array(img)
    results = model.detect([img_arr], verbose=1)
    r = results[0]

    # r = "hello everyone"

    return {"results": json.dumps(r, cls=NumpyEncoder)}
