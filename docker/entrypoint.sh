#!/bin/sh

alias activate=". /home/mask_rcnn/venv/bin/activate"

cd /home/mask_rcnn
su mask_rcnn
python3.7 -m venv venv
activate
cd application
pip install -r requirements.txt
pip install gunicorn
touch /var/log.txt
chmod a+rw /var/log.txt

gunicorn app:app -b 0.0.0.0:5098 -w 1  --log-file=- >> /var/log.txt 2>&1
